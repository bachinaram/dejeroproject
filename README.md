---
title: The API client Assignment
---

# Your Project Name - Assignment


Welcome to the technical assessment! Please refer to the corresponding prompt dependent on your desired role. These prompts are not intended to be difficult or time intensive, rather provide an applicable reference to your implementation practices. An ideal response is not only functionally complete, but is well documented and easy to maintain. 

## DevOps Specialist
Design and implement a functional HTTPS API client for this webserver. Please consider, and implement, a design pattern that satisfies a variety of usecases. If you strive to impress, feel free to attempt the Sn. DevOps Specialist assessment. This assessment is expected to take 1 - 4 hours to complete and to be less than 100 lines of code. 

    Client Class (APIClient):
        The APIClient class is designed to interact with a RESTful API using various HTTP methods.
        It requires a base URL (resturl) and an authentication token (authToken).

    Factory Method (createRequest):
        The createRequest method is a factory method responsible for creating instances of APIRequest based on the provided HTTP method.
        It takes parameters such as the method, endpoint, query parameters, and authentication credentials (username and password).
        The endpoint is appended to the base URL, and query parameters are added if present.
        The method then dynamically creates the appropriate APIRequest object based on the HTTP method.

    APIRequest Interface:
        The APIRequest interface defines the common methods for executing HTTP requests (execute method).

    Concrete Implementations (GETRequest, POSTRequest, PUTRequest, DELETERequest):
        Each HTTP method has its concrete implementation of the APIRequest interface.
        The implementations encapsulate the details of constructing and executing HTTP requests for their respective methods.
        GETRequest, POSTRequest, PUTRequest, and DELETERequest are examples of these concrete implementations.

    Main Method (main):
        The main method in the APIClient class demonstrates the usage of the factory method and different HTTP requests.
        It creates an instance of APIClient with the base URL and authentication token.
        It then uses the factory method to create instances of APIRequest for GET, POST, PUT, and DELETE operations.
        Finally, it executes each request and prints the response body.

    Scalability and Extensibility:
        The design allows for easy extension by adding new implementations of APIRequest for additional HTTP methods.
        The factory method abstracts away the instantiation details, promoting code scalability and flexibility.

    Exception Handling:
        The design includes exception handling for unsupported HTTP methods and potential errors during request execution.

    Parameters and Authentication:
        The factory method accepts parameters like query parameters and authentication credentials, providing flexibility in constructing requests.

    Dynamic URL Construction:
        The design dynamically constructs the complete URL by combining the base URL, endpoint, and query parameters.

    Authentication Token Handling:
        The authentication token is stored as an instance variable and can be easily modified or extended for more sophisticated authentication mechanisms.


GIT CICD is used to build the code with the help of maven build tool which builds in the first phase of cicd process and later the plan is to create aws resources in another job to host the jar in the load balancing ec2 instances as described below.


## Senior DevOps Specialist

Design and implement a replicant HTTPS API server using the cloud technologies of your choosing. Please consider, and implement, technologies that are representative of how you would build, and maintain, this server in a production environment (e.g. TLS termination). If you require a compute resource, please reach out to the hiring manager and one will be provided for you. This assessment is expected to take 4 - 8 hours to complete and to be less than 500 lines of code. 

    VPC Setup:
        A Virtual Private Cloud (VPC) is created with a specified CIDR block (10.0.0.0/16).
        Two subnets (MySubnet1 and MySubnet2) are created in different availability zones within the VPC.

    Internet Gateway and NAT Gateway:
        An Internet Gateway (MyInternetGateway) is created and attached to the VPC for public internet access.
        A NAT Gateway (MyNATGateway) is created in MySubnet1 for instances in private subnets to access the internet.

    Elastic IP (EIP):
        An Elastic IP (MyEIP) is created to associate with the NAT Gateway for a stable public IP address.

    Security Groups:
        Two security groups are created (MyALBSecurityGroup and MyASGSecurityGroup).
        MyALBSecurityGroup allows traffic on port 80 from anywhere (0.0.0.0/0) for the Application Load Balancer (ALB).
        MyASGSecurityGroup allows traffic on port 80 from the security group of the ALB (MyALBSecurityGroup) for instances in the Auto Scaling Group (ASG).

    Application Load Balancer (ALB):
        An Application Load Balancer (MyALB) is created in the specified subnets and associated with MyALBSecurityGroup.

    ALB Listener:
        An ALB listener (MyALBListener) is configured to respond with a fixed "OK" message on port 80.

    Target Group:
        A Target Group (MyTargetGroup) is created to define how the ALB routes traffic to instances.

    Launch Configuration:
        A Launch Configuration (MyLaunchConfig) is defined for instances in the Auto Scaling Group.
        It uses a specific Amazon Machine Image (AMI) and installs Apache HTTP server. It also downloads a JAR file and moves it to the web server directory.

    Auto Scaling Group (ASG):
        An Auto Scaling Group (MyASG) is created to manage a group of instances.
        It uses the defined launch configuration, spans multiple availability zones, and is associated with the target group.

    Outputs:
        An output (LoadBalancerDNS) provides the DNS name of the ALB, which can be used to access the application.


## Key Features 

- Authentication: [Requests must include an AUTHTOKEN header. This token will have been provided to you alongside the initial reference to this webserver. Client requests to authenticated endpoints (i.e. every endpoint except this one) without a valid token will return a 401 (Unauthorized) response. ]
- API End Points: [This webserver hosts a single API endpoint, /v1/user. The this endpoint is scoped with a user_id, e.g. /v1/user?user_id=INT. The following methods are supported: POST, PUT, GET, DELETE. Requests are scoped with a valid user_id, with the exception of POST, which returns the user_id in the request response object. ]

{'users': [{
   "user_id": INT,
   "username": STRING,
   "password": SHA1(USERNAME+PASSWORD)}],
 'errors': []}

DELETE requests will be of the form

{'users': [], 'errors': []}
- ...

## Methods to implement

1. POST: [ Create a user. The POST body must be of the form

{"username": "STRING", "password": "STRING"}]
2. PUT: [ Update a user. The PUT body must be of the form

{"username": "STRING", "password": "STRING"}]
3. GET: [Return a user. If the user_id parameter is omitted, all users are returned.  ]
4. DELETE: [Delete a user. ]
