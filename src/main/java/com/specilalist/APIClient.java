package com.specilalist;

import java.net.http.HttpResponse;
import java.util.Map;

class APIClient {
    private final String authToken;
    private final String resturl;
    APIClient(String resturl, String authToken) {
        this.resturl = resturl;
        this.authToken = authToken;
    }

    APIRequest createRequest(String method, String endpoint, Map<String, String> params, String username, String password) throws Exception {
        String url = resturl + "/" + endpoint;

        if (params != null && !params.isEmpty()) {
            StringBuilder queryParams = new StringBuilder();
            params.forEach((key, value) -> queryParams.append(key).append("=").append(value).append("&"));
            url += "?" + queryParams.toString();
        }

        switch (method.toUpperCase()) {
            case "GET":
                return new GETRequest(url);
            case "POST":
                return new POSTRequest(url, username, password);
            case "PUT":
                return new PUTRequest(url, username, password);
            case "DELETE":
                return new DELETERequest(url);
            default:
                throw new IllegalArgumentException("Unsupported HTTP method: " + method);
        }
    }

    public static void main(String[] args) {
        //String resturl = "https://samplewebsite.com/api";
        //String resturl = "https://api.sunrise-sunset.org";
        String resturl = "https://jsonplaceholder.typicode.com";
        String authToken = "FJkldafn3nV983f";

        APIClient apiClient = new APIClient(resturl, authToken);

        try {
            APIRequest getRequest = apiClient.createRequest("GET", "v1/user", Map.of("user_id", "123"), null, null);
            //APIRequest getRequest = apiClient.createRequest("GET", "json", Map.of("lat", "36.7201600"), null, null);
            //APIRequest getRequest = apiClient.createRequest("GET", "posts/1", Map.of("", "1"), null, null);
            HttpResponse<String> getResponse = getRequest.execute();
            System.out.println(getResponse.body());
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            APIRequest postRequest = apiClient.createRequest("POST", "v1/user", null, "new_user", "password123");
            HttpResponse<String> postResponse = postRequest.execute();
            System.out.println(postResponse.body());
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            APIRequest putRequest = apiClient.createRequest("PUT", "v1/user", null, "updated_user", "updated_password");
            HttpResponse<String> putResponse = putRequest.execute();
            System.out.println(putResponse.body());
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            //APIRequest deleteRequest = apiClient.createRequest("DELETE", "posts/1", Map.of("userid", "1"), null, null);
            APIRequest deleteRequest = apiClient.createRequest("DELETE", "v1/user", Map.of("user_id", "456"), null, null);
            HttpResponse<String> deleteResponse = deleteRequest.execute();
            System.out.println(deleteResponse.body());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
}