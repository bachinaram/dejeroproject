package com.specilalist;

import java.net.http.HttpResponse;

interface APIRequest {
    HttpResponse<String> execute() throws Exception;
}
