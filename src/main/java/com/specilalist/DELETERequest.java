package com.specilalist;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

class DELETERequest implements APIRequest {
    private final String url;

    DELETERequest(String url) {
        this.url = url;
    }

    @Override
    public HttpResponse<String> execute() throws Exception {
        return HttpClient.newHttpClient().send(HttpRequest.newBuilder().uri(URI.create(url)).DELETE().build(),
                HttpResponse.BodyHandlers.ofString());
    }
}