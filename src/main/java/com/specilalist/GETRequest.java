package com.specilalist;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

class GETRequest implements APIRequest {
    private final String url;

    GETRequest(String url) {
        this.url = url;
    }

    @Override
    public HttpResponse<String> execute() throws Exception {
        return HttpClient.newHttpClient().send(HttpRequest.newBuilder().uri(URI.create(url)).GET().build(),
                HttpResponse.BodyHandlers.ofString());
    }
}