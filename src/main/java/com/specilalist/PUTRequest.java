package com.specilalist;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;

class PUTRequest implements APIRequest {
    private final String url;
    private final String username;
    private final String password;

    PUTRequest(String url, String username, String password) throws Exception {
        this.url = url;
        this.username = username;
        this.password = hashPassword(username, password);
    }

    @Override
    public HttpResponse<String> execute() throws Exception {
        String putData = String.format("{\"username\": \"%s\", \"password\": \"%s\"}", username, password);
        return HttpClient.newHttpClient().send(
                HttpRequest.newBuilder().uri(URI.create(url)).header("Content-Type", "application/json")
                        .PUT(HttpRequest.BodyPublishers.ofString(putData)).build(),
                HttpResponse.BodyHandlers.ofString());
    }

    private String hashPassword(String username, String password) throws Exception {
        String input = username + password;
        MessageDigest digest = MessageDigest.getInstance("SHA-1");
        byte[] hashedBytes = digest.digest(input.getBytes(StandardCharsets.UTF_8));
        return bytesToHex(hashedBytes);
    }

    private String bytesToHex(byte[] bytes) {
        StringBuilder result = new StringBuilder();
        for (byte b : bytes) {
            result.append(String.format("%02x", b));
        }
        return result.toString();
    }
}
