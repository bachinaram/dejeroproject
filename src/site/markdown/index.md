---
title: The API client Assignment
---

# Your Project Name - Assignment for DevOps Specialist


Welcome to the technical assessment! Please refer to the corresponding prompt dependent on your desired role. These prompts are not intended to be difficult or time intensive, rather provide an applicable reference to your implementation practices. An ideal response is not only functionally complete, but is well documented and easy to maintain. 

## DevOps Specialist
Design and implement a functional HTTPS API client for this webserver. Please consider, and implement, a design pattern that satisfies a variety of usecases. If you strive to impress, feel free to attempt the Sn. DevOps Specialist assessment. This assessment is expected to take 1 - 4 hours to complete and to be less than 100 lines of code. 


## Key Features 

- Authentication: [Requests must include an AUTHTOKEN header. This token will have been provided to you alongside the initial reference to this webserver. Client requests to authenticated endpoints (i.e. every endpoint except this one) without a valid token will return a 401 (Unauthorized) response. ]
- API End Points: [This webserver hosts a single API endpoint, /v1/user. The this endpoint is scoped with a user_id, e.g. /v1/user?user_id=INT. The following methods are supported: POST, PUT, GET, DELETE. Requests are scoped with a valid user_id, with the exception of POST, which returns the user_id in the request response object. ]

{'users': [{
   "user_id": INT,
   "username": STRING,
   "password": SHA1(USERNAME+PASSWORD)}],
 'errors': []}

DELETE requests will be of the form

{'users': [], 'errors': []}
- ...

## Methods to implement

1. POST: [ Create a user. The POST body must be of the form

{"username": "STRING", "password": "STRING"}]
2. PUT: [ Update a user. The PUT body must be of the form

{"username": "STRING", "password": "STRING"}]
3. GET: [Return a user. If the user_id parameter is omitted, all users are returned.  ]
4. DELETE: [Delete a user. ]

